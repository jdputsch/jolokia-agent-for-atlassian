<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <groupId>sk.eea.jolokia</groupId>
    <artifactId>jolokia-agent-for-atlassian</artifactId>
    <version>0.6.0</version>

    <organization>
        <name>EEA s.r.o., Slovakia</name>
        <url>http://www.eea.sk/</url>
    </organization>

    <name>Jolokia Agent for Atlassian</name>
    <description>Jolokia Agent packaged for Atlassian products as servlet</description>
    <packaging>atlassian-plugin</packaging>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <amps.version>8.0.0</amps.version>
        <atlassian.rest.version>3.0.14</atlassian.rest.version>

        <jolokia.version>1.6.2</jolokia.version>

        <bamboo.version>RELEASE</bamboo.version>
        <bamboo.version.min>5.9.0</bamboo.version.min>
        <bitbucket.version>RELEASE</bitbucket.version>
        <bitbucket.version.min>4.0.0</bitbucket.version.min>
        <confluence.version>RELEASE</confluence.version>
        <confluence.version.min>5.7</confluence.version.min>
        <jira.version>RELEASE</jira.version>
        <jira.version.min>6.4</jira.version.min>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.jolokia</groupId>
            <artifactId>jolokia-core</artifactId>
            <version>${jolokia.version}</version>
        </dependency>

        <!-- Atlassian platform dependencies -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <version>2.4</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.sal</groupId>
            <artifactId>sal-api</artifactId>
            <version>2.7.0</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins.rest</groupId>
            <artifactId>atlassian-rest-common</artifactId>
            <version>${atlassian.rest.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.ws.rs</groupId>
            <artifactId>jsr311-api</artifactId>
            <version>1.0</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jaxb-api</artifactId>
            <version>2.1</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>1.7.25</version>
            <scope>provided</scope>
        </dependency>

        <!-- Test dependencies -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins.rest</groupId>
            <artifactId>atlassian-rest-module</artifactId>
            <version>${atlassian.rest.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.6</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>amps-maven-plugin</artifactId>
                <version>${amps.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <jvmArgs>-Xmx1024m -Duser.language=en -Duser.country=US</jvmArgs>
                    <allowGoogleTracking>false</allowGoogleTracking>
                    <enableDevToolbox>false</enableDevToolbox>
                    <enablePde>false</enablePde>
                    <extractDependencies>false</extractDependencies>
                    <skipRestDocGeneration>true</skipRestDocGeneration>
                    <skipManifestValidation>true</skipManifestValidation>
                    <products>
                        <product>
                            <id>bamboo</id>
                            <instanceId>bamboo</instanceId>
                            <version>${bamboo.version}</version>
                            <ajpPort>8010</ajpPort>
                        </product>
                        <product>
                            <id>bamboo</id>
                            <instanceId>bamboo-min</instanceId>
                            <version>${bamboo.version.min}</version>
                            <ajpPort>8010</ajpPort>
                        </product>
                        <product>
                            <id>bitbucket</id>
                            <instanceId>bitbucket</instanceId>
                            <version>${bitbucket.version}</version>
                            <ajpPort>8011</ajpPort>
                        </product>
                        <product>
                            <id>bitbucket</id>
                            <instanceId>bitbucket-min</instanceId>
                            <version>${bitbucket.version.min}</version>
                            <ajpPort>8011</ajpPort>
                        </product>
                        <product>
                            <id>confluence</id>
                            <instanceId>confluence</instanceId>
                            <version>${confluence.version}</version>
                            <ajpPort>8012</ajpPort>
                        </product>
                        <product>
                            <id>confluence</id>
                            <instanceId>confluence-min</instanceId>
                            <version>${confluence.version.min}</version>
                            <ajpPort>8012</ajpPort>
                        </product>
                        <product>
                            <id>jira</id>
                            <instanceId>jira</instanceId>
                            <version>${jira.version}</version>
                            <ajpPort>8013</ajpPort>
                        </product>
                        <product>
                            <id>jira</id>
                            <instanceId>jira-min</instanceId>
                            <version>${jira.version.min}</version>
                            <ajpPort>8013</ajpPort>
                        </product>
                    </products>
                    <testGroups>
                        <testGroup>
                            <id>backward-compatibility</id>
                            <instanceIds>
                                <instanceId>bamboo-min</instanceId>
                                <instanceId>bitbucket-min</instanceId>
                                <instanceId>confluence-min</instanceId>
                                <instanceId>jira-min</instanceId>
                            </instanceIds>
                            <includes>
                                <include>it/**/*Test.java</include>
                            </includes>
                        </testGroup>
                        <testGroup>
                            <id>release-tests</id>
                            <instanceIds>
                                <instanceId>bamboo</instanceId>
                                <instanceId>bitbucket</instanceId>
                                <instanceId>confluence</instanceId>
                                <instanceId>jira</instanceId>
                            </instanceIds>
                            <includes>
                                <include>it/**/*Test.java</include>
                            </includes>
                        </testGroup>
                    </testGroups>
                    <instructions>
                        <Import-Package>
                            *;version="0";resolution:=optional
                        </Import-Package>
                    </instructions>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>

