package sk.eea.jolokia;

import org.jolokia.backend.executor.MBeanServerExecutor;
import org.jolokia.detector.ServerHandle;
import org.jolokia.detector.TomcatDetector;

public class Detector extends TomcatDetector {
    @Override
    public ServerHandle detect(MBeanServerExecutor pMBeanServerExecutor) {
        return new ServerHandle("Atlassian", Service.appName, Service.appVersion, Service.extraInfo);
    }
}
