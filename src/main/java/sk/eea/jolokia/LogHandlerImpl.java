package sk.eea.jolokia;

import org.jolokia.util.LogHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogHandlerImpl implements LogHandler {

    private final Logger logger = LoggerFactory.getLogger("org.jolokia");

    @Override
    public void debug(String s) {
        logger.debug(s);
    }

    @Override
    public void info(String s) {
        logger.info(s);
    }

    @Override
    public void error(String s, Throwable throwable) {
        logger.error(s, throwable);
    }
}
