package sk.eea.jolokia;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.jolokia.config.ConfigKey;
import org.jolokia.restrictor.PolicyRestrictor;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class Service {
    private static final String KEY_ACCESS_XML = "plugin.jolokia.access.xml";
    private static final String KEY_CONFIG = "plugin.jolokia.config";
    private final PluginSettings globalSettings;

    private boolean configurationLoaded;

    // TODO AplicationProperties are not available in Detector?
    static String appName = "?";
    static String appVersion = "?";
    static Map<String, String> extraInfo = null;

    public Service(PluginSettingsFactory pluginSettingsFactory, ApplicationProperties applicationProperties) {
        globalSettings = pluginSettingsFactory.createGlobalSettings();
        appName = applicationProperties.getDisplayName();
        appVersion = applicationProperties.getVersion();
        extraInfo = new HashMap<String, String>();
        extraInfo.put("baseUrl", applicationProperties.getBaseUrl());
        extraInfo.put("buildNumber", applicationProperties.getBuildNumber());
    }

    public PolicyRestrictor getPolicyRestrictor() {
        String access_xml = getAccessXml();
        if (access_xml != null) {
            try {
                return new PolicyRestrictor(new ByteArrayInputStream(access_xml.getBytes(StandardCharsets.UTF_8)));
            } catch (SecurityException e) {
                // FIXME store me error message
            }
        }
        return new PolicyRestrictor(getDefaultAccessXmlAsStream());
    }

    private InputStream getDefaultAccessXmlAsStream() {
        return this.getClass().getResourceAsStream("/jolokia-access.xml");
    }

    private String readFromInputStream(InputStream inputStream) throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }

    public String getDefaultAccessXml() {
        try {
            return readFromInputStream(getDefaultAccessXmlAsStream());
        } catch (IOException e) {
            return "";
        }
    }

    public String getAccessXml() {
        return (String) globalSettings.get(KEY_ACCESS_XML);
    }

    public void setAccessXml(String value) {
        if (value == null) {
            globalSettings.remove(KEY_ACCESS_XML);
        } else {
            globalSettings.put(KEY_ACCESS_XML, value);
        }
    }

    public boolean isConfigurationLoaded() {
        return configurationLoaded;
    }

    public void setConfigurationLoaded(boolean configurationLoaded) {
        this.configurationLoaded = configurationLoaded;
    }

    public Map<String,String> getSettings() {
        HashMap<String, String > settings = new HashMap<>();
        for (ConfigKey configKey: ConfigKey.values()) {
            Object value = globalSettings.get(KEY_CONFIG +"."+configKey.getKeyValue());
            if (configKey.isGlobalConfig() && value != null) {
                settings.put(configKey.getKeyValue(), value.toString());
            }
        }
        return settings;
    }

    public void setSettings(Map<String,String> settings) {
        for (String key: settings.keySet()) {
            if (ConfigKey.getGlobalConfigKey(key) != null) {
                final String ref = KEY_CONFIG + "." + key;
                if (settings.get(key) == null) {
                    globalSettings.remove(ref);
                } else {
                    globalSettings.put(ref, settings.get(key));
                }
            }
            // TODO warn unknown keys
        }
    }
}