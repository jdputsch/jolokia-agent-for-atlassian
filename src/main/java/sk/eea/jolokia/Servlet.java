package sk.eea.jolokia;

import org.jolokia.http.AgentServlet;
import org.jolokia.util.LogHandler;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Map;

public class Servlet extends AgentServlet {
    private final Service service;

    /**
     * Initializes servlet with configured policy restrictor
     */
    public Servlet(Service service) {
        super(service.getPolicyRestrictor());
        service.setConfigurationLoaded(true);
        this.service = service;
    }

    @Override
    public void init(ServletConfig pServletConfig) throws ServletException {
        super.init(new MyServletConfig(pServletConfig, service.getSettings()));
    }

    @Override
    protected LogHandler createLogHandler(ServletConfig pServletConfig, final boolean pDebug) {
        return new LogHandlerImpl();
    }

    public static class MyServletConfig implements ServletConfig {
        private final ServletConfig config;
        private final Map<String, String> settings;

        MyServletConfig(final ServletConfig config, final Map<String, String> settings) {
            this.config = config;
            this.settings = settings;
        }

        @Override
        public String getServletName() {
            return config.getServletName();
        }

        @Override
        public ServletContext getServletContext() {
            return config.getServletContext();
        }

        @Override
        public String getInitParameter(String s) {
            return settings.get(s);
        }

        @Override
        public Enumeration getInitParameterNames() {
            return Collections.enumeration(settings.keySet());
        }
    }
}
