package it.sk.eea.jolokia;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

public class HttpAccessibilityTest {

    private final String url = "http://localhost:"+System.getProperty("http.port")+System.getProperty("context.path");
    private final Client client = Client.create();
    private String authorization = "Basic " +
            Base64.getEncoder().encodeToString("admin:admin".getBytes(StandardCharsets.UTF_8));

    @Test
    public void testAccessAPI() {
        ClientResponse response = client.resource(url+"/rest/jolokia/1.0/access")
                .accept(MediaType.TEXT_XML)
                .header(HttpHeaders.AUTHORIZATION, authorization)
                .get(ClientResponse.class);

        assertEquals(200, response.getStatus());
        // TODO check response
    }

    @Test
    public void testAnonAccessAPI() {
        ClientResponse response = client.resource(url+"/rest/jolokia/1.0/access")
                .accept(MediaType.TEXT_XML)
                .get(ClientResponse.class);

        assertEquals(401, response.getStatus());
        // TODO check response
    }

    @Test
    public void testUpdateAPI() throws IOException {
        final InputStream resource = this.getClass().getClassLoader().getResourceAsStream("test-jolokia-access.xml");
        assertNotNull(resource);
        ClientResponse response = client.resource(url+"/rest/jolokia/1.0/access")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_XML)
                .header(HttpHeaders.AUTHORIZATION, authorization)
                .put(ClientResponse.class, IOUtils.toString(resource));

        assert(response.getStatus() == 302 || response.getStatus() == 202);
    }

    @Test
    public void testJolokiaVersionGet() {
        ClientResponse response = client.resource(url+"/plugins/servlet/jolokia/version")
                .accept(MediaType.APPLICATION_JSON)
                .get(ClientResponse.class);

        assertEquals(200, response.getStatus());
        // TODO check response
    }


    @Test
    public void testJolokiaVersionPost() {
        ClientResponse response = client.resource(url+"/plugins/servlet/jolokia")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.CONTENT_ENCODING, MediaType.APPLICATION_JSON)
                .post(ClientResponse.class, "{\"type\": \"version\"}");

        assertEquals(200, response.getStatus());
        // TODO check response
    }

    @Test
    public void testJolokiaStacktrace() {

    }
}
